import 'package:flutter/material.dart';

void main() => runApp(WeatherPage());

class WeatherPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        //appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        // body: Container(
        //   color: Colors.cyan[50],
        // ),
      ),
    );

  }

  Widget buildMapButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.map_outlined,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
      ],
    );
  }
  Widget buildLocationButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.location_on,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
      ],
    );
  }
  Widget buildMenuButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.menu,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
      ],
    );
  }

  Widget buildBodyWidget(){
    return ListView (
      children: <Widget>[

        Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.all(32),height: 700,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: NetworkImage("https://external-preview.redd.it/JI_xq_yzYB2iAGfZ_nWV0pw6WHIHnYsd7Oi-GmDMRgQ.jpg?auto=webp&s=2e24ad0998e441540e3a5ec8f84f07cfce6ea630"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
              children: <Widget>[
                Text('อ.เมืองชลบุรี',
                  style: TextStyle(
                      fontSize: 35,color: Colors.white
                  ),),
                Text('26°',
                  style: TextStyle(
                      fontSize: 100,color: Colors.white
                  ),),
                Text('เมฆเป็นส่วนมาก',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,color: Colors.white
                  ),),
                Text('สูงสุด: 32° ต่ำสุด: 21°',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,color: Colors.white

                  ),),

                const SizedBox(
                  height: 20,
                ),
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.all(0),
                  height: 150,
                  width:double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.black38,
                    borderRadius: BorderRadius.circular(20), //border corner radius
                    boxShadow:[
                      BoxShadow(
                        color: Colors.white.withOpacity(0), //color of shadow
                        spreadRadius: 5, //spread radius
                        blurRadius: 7, // blur radius
                        offset: Offset(0, 2), // changes position of shadow

                      ),
                    ],
                  ),
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('คาดว่ามีเมฆบางส่วนประมาณเวลา 23.00',
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            height:2.3,
                          ),
                        ),
                        Divider(
                          indent: 20,
                          thickness: 0,
                          color: Colors.white,
                        )
                        ,timeTtems(),
                        iconTtems(),
                      ]
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  margin: EdgeInsets.all(0),
                  height:210,
                  width:double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.black38,
                    borderRadius: BorderRadius.circular(20), //border corner radius
                    boxShadow:[
                      BoxShadow(
                        color: Colors.white.withOpacity(0), //color of shadow
                        spreadRadius: 5, //spread radius
                        blurRadius: 7, // blur radius
                        offset: Offset(0, 2), // changes position of shadow

                      ),
                    ],
                  ),
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        buildCalendarButton(),
                        Divider(
                          indent: 20,
                          thickness: 0,
                          color: Colors.white,
                        ),
                        buildWeatherToday(),
                        buildWeatherTuesday(),
                        buildWeatherWednesday(),
                      ]
                  ),


                ),
              ]
          ),
        ),

        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Container(
              child: Wrap(
                spacing: 120,
                children: <Widget>[
                  buildMapButton(),
                  buildLocationButton(),
                  buildMenuButton(),
                ],
              ),
            ),

          ],
        ),
      ],
    );
  }
}
Widget buildWeatherWednesday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('พฤ.   ',style: TextStyle(
            color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°',style: TextStyle(
            color: Colors.white)),
        Text('🌡️',style: TextStyle(
            color: Colors.deepOrange)),
        Text('31°',style: TextStyle(
            color: Colors.white)),

      ]
  );
}
Widget buildWeatherTuesday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('พ.   ',style: TextStyle(
            color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°',style: TextStyle(
            color: Colors.white)),
        Text('🌡️',style: TextStyle(
            color: Colors.deepOrange)),
        Text('32°',style: TextStyle(
            color: Colors.white)),

      ]
  );
}

Widget buildWeatherToday(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('วันนี้',style: TextStyle(
            color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°',style: TextStyle(
            color: Colors.white)),
        Text('🌡️',style: TextStyle(
            color: Colors.deepOrange)),
        Text('32°',style: TextStyle(
            color: Colors.white)),

      ]
  );
}
Widget timeTtems(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('21',style: TextStyle(
            color: Colors.white)),
        Text('22',style: TextStyle(
            color: Colors.white)),
        Text('23',style: TextStyle(
            color: Colors.white)),
        Text('00',style: TextStyle(
            color: Colors.white)),
        Text('01',style: TextStyle(
            color: Colors.white)),
        Text('02',style: TextStyle(
            color: Colors.white)),
      ]
  );
}
Widget buildCalendarButton() {
  return Row(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.calendar_month,
          color: Colors.white,
          size: 20,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('พยากรณ์อากาศ 10 วัน',
        style: TextStyle(
          fontSize: 15,
          color: Colors.white,
          height:0,
          fontWeight: FontWeight.bold,
        ),
      )
    ],
  );
}

Widget buildCloud28Button() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('26°',style: TextStyle(
          color: Colors.white)),
    ],
  );
}
Widget buildCloud29Button() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('26°',style: TextStyle(
          color: Colors.white)),
    ],
  );
}

Widget buildCloud30Button() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('25°',style: TextStyle(
          color: Colors.white)),
    ],
  );
}
Widget buildCloud27Button() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.cloud_rounded,
          color: Colors.white,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('27°',style: TextStyle(
          color: Colors.white)),
    ],
  );
}
Widget iconTtems(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        buildCloud28Button(),
        buildCloud29Button(),
        buildCloud30Button(),
        buildCloud29Button(),
        buildCloud29Button(),
        buildCloud27Button(),
      ]
  );
}
